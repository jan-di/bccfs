# Revision 25
* Removed version number. Using only Revision number from now on.
* Built a Drag-n-Drop-automatic-language-detection-loader... Thingy... Which will detect the language the game uses, find the appropriate language file in BCCFS and load it. If the language file is missing, it'll just load English instead.

## Revision 23 & 24
* Added a fail safe to a function that could cause the game to crash.

### Revision 22
* Removed some unused variables
* Removed some source doc comments
* Removed unnecessary hook & call
* Removed localization cache. Load localization data directly, instead of via the aforementioned hook.

### Revision 21
* Switched to automatic language detection instead of using menu options
* Remove some whitespace
* Removed a bit of clutter and unused variables

### Revision 20
* Russian translation added, courtesy of chrom[K]a
* Other changes in Rev 20 for people interested in the code can be found @ https://bitbucket.org/Selfrot/bccfs/commits/all
