{
	"name" : "Bain-Cant-Cook-For-Shit",
	"description" : "Displays the ingredient that should be added to the methlab in the chat or as a waypoint.",
	"author" : "Selfrot",
	"contact" : "selfrotlol@gmail.com",
	"updates" : [
		{
			"revision" : 25,
			"identifier" : "BCCFS"
		}
	],
	"hooks" : [
		{"hook_id" : "lib/managers/menumanager",   "script_path" : "menu/menu.lua"},
		{"hook_id" : "lib/managers/dialogmanager", "script_path" : "lua/bccfs.lua"},
		{"hook_id" : "lib/managers/menumanager",   "script_path" : "lua/variables.lua"}
	]
}
