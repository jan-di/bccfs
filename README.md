![](http://p3bble.com/cdn1/imgs/bccfs_banner.jpg)

### Description (Short version) ###
Depending on what settings you choose to use it will; add a waypoint to the meth-lab receptacle (Mu cooker, Cs cooler or Hcl bucket) and/or spit out the correct ingredient in the chat.

### Features ###
* Print the ingredient in the chat--If you want it to.
* Place a waypoint on the receptacle (See picture below). Again; if you want it to.
* The ability to turn off chat messages & waypoints at any point. Changing the settings in-game will take effect immediately. No need to reload.
* Only you can see the chat messages, and it will only send the ingredient once. It won't spam you or your crew over and over again when you're otherwise occupied dealing with the hoard of cops outside.
* The waypoint will stay until an ingredient has been added/the lab is destroyed. On Lab Rats, it will stay until all 3 bags of ingredients have been added

###### [Download](http://download.paydaymods.com/download/latest/BCCFS)
![](http://p3bble.com/cdn1/imgs/options-preview.jpg)
![Screenshot showing both the waypoint and chat message](http://images.akamai.steamusercontent.com/ugc/647751090590447775/7B2D36F96363EA0B39005E9AF79B1F9E0BEDCC43/)

### How to install ###
First step is quite obvious, but; download the latest version of BCCFS.

Extract the BCCFS folder directly into BLT's "mods" folder, so it looks something like this;

* /Steam/steamapps/common/Payday 2/mods
* ---- /BCCFS
* ---- ---- /mod.txt
* ---- ---- /lua/
* ---- ---- ---- /bccfs.lua
* ---- ---- ---- /variables.lua
* ---- ---- /menu/
* ---- ---- ---- /en.txt
* ---- ---- ---- /menu.lua
* ---- ---- ---- /menu_options.txt

The file that stores your menu options will be automatically created under ".../Payday 2/mods/saves/bccfs_saved_data.txt". You will most likely never have to deal with this file yourself, but I thought I'd mention it anyways.

### Known issues ###
1) If you turn off waypoints while a waypoint is still active it will stay there until an ingredient has been added or the lab is torched. Once that happens the waypoints will be fully turned off. [Repo Issues](https://bitbucket.org/Selfrot/bccfs/issues?status=new&status=open)

### Description (Long version) ###
A simple mod that translates Bain's encrypted dialogs into plain English.
He's on his computer, reading the recipe off the screen - but still, after thousands of batches cooked; he can't even remember the names of the ingredients!

This mod is injected directly into Bain's nervous system (Using alien technology or something, I dunno...) which; while allowing him to function as normal (i.e; repeat the same things over and over, give you bad directions, etc) -- it will force him to text you the correct ingredient.

###### Warning: BCCFS nanobots are experimental. Side effects include but are not limited to; lots of meth to be shared with the world, decreased exposure to chemical burns, decreased frustration in general... *cough* *clears throat* Can also lead to spontaneous explosion of Bain or Bain-like humanoids. ######

###### [Download](http://download.paydaymods.com/download/latest/BCCFS)