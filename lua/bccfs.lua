BCCFSClass = BCCFSClass or class()
BCCFSClass.Common = _G.BCCFSCommon

BCCFSClass.SessionVariables = {
      -- General variables
      ["initialized"] = false,
      ["current_heist"] = nil,
      
      -- Rats specific
      ["object_positions"] = {
            ["mu"] = nil,
            ["cs"] = nil,
            ["hcl"] = nil
      }
}

BCCFSClass.MethBatch = {
      ["muriatic_acid"] = false,
      ["caustic_soda"] = false,
      ["hydrogen_chloride"] = false,
      
      ["last_ingredient_added"] = nil
}
local session = BCCFSClass.SessionVariables
local idstring = BCCFSClass.Common.Variables.object_ids

--[[
-- Ripped from Pocohud
-- Amazing mod by Zenyr. If you don't have it already; what's wrong with you? =P
-- Pocomods @ Github zenyr/PocoHud3
]]
function BCCFSClass:SendChatMessage(name, message, color)
      if not message then
            message = name
            name = nil
      end
      if not tostring(color):find('Color') then
            color = nil
      end
      message = tostring(message)
      if managers and managers.chat and managers.chat._receivers and managers.chat._receivers[1] then
            for __, rcvr in pairs(managers.chat._receivers[1]) do
                  rcvr:receive_message(name or "*", message, color or tweak_data.chat_colors[5]) 
            end  
      end
end
function BCCFSClass:print_ingredient(id)
      local color_data = BCCFSMenu._data
      local rh, gs, bv = color_data.RH, color_data.GS, color_data.BV
      BCCFSClass:SendChatMessage("[BCCFS]", id, Color(1, rh, gs, bv))
end

--[[
-- Ripped from Goonmod
-- James and SirWaddle, you guys are awesome!!!
-- http://paydaymods.com
]]
function BCCFSClass:_add_waypoint(name, position)
      if name and position then
            local color_data = BCCFSMenu._data
            local rh, gs, bv = color_data.RH, color_data.GS, color_data.BV
            managers.hud:add_waypoint(
            'CookingWaypoint_CookThis_' .. name, {
                  icon = 'equipment_vial',
                  distance = true,
                  position = position:ToVector3(),
                  no_sync = true,
                  present_timer = 0,
                  state = "present",
                  radius = 50,
                  color = Color(1, rh, gs, bv),
                  blend_mode = "add"
            })
      end
end
function BCCFSClass:_remove_waypoint(id)
      managers.hud:remove_waypoint("CookingWaypoint_CookThis_" .. id)
end

function BCCFSClass:_correct_position(vector, isHcl)
      local newPosition = {["x"] = nil,["y"] = nil,["z"] = nil}
      local _return
      if vector then
          for vector in string.gmatch(vector, '([^,]+)') do
                local _num = tonumber(vector)
                
                if newPosition.x == nil then
                      newPosition.x = isHcl and _num - 18 or _num
                      
                elseif newPosition.y == nil then
                      newPosition.y = isHcl and _num + 10 or _num
                      
                elseif newPosition.z == nil then
                      newPosition.z = _num + 30
                end
          end
          
          _return = newPosition.x .. "," .. newPosition.y .. "," .. newPosition.z -- String "x,y,z"
      end

      return _return
end

function BCCFSClass:generate_positions()
      if managers.interaction then
            local positions = { ["mu"] = nil, ["cs"] = nil, ["hcl"] = nil }
            
            for _,v in pairs(managers.interaction._interactive_units) do
                  local object = string.sub(v:interaction()._unit:name():t(), 1, 10)
                  
                  positions.mu = object == idstring.muriatic_acid and Vector3.ToString(v:interaction():interact_position()) or positions.mu
                  positions.cs = object == idstring.caustic_soda and Vector3.ToString(v:interaction():interact_position()) or positions.cs
                  positions.hcl = object == idstring.hydrogen_chloride and Vector3.ToString(v:interaction():interact_position()) or positions.hcl
            end
            
            return positions
      end
end

function BCCFSClass:init()      
      if managers.job then
            local heist = managers.job:current_job_id()
            
            if heist then
                  if heist == "rat" or heist == "alex" or heist == "alex_prof" then
                        local generated_positions = BCCFSClass:generate_positions()     
                                           
                        if generated_positions then
                              session.object_positions.mu = BCCFSClass:_correct_position(generated_positions.mu, false)
                              session.object_positions.cs = BCCFSClass:_correct_position(generated_positions.cs, false)
                              session.object_positions.hcl = BCCFSClass:_correct_position(generated_positions.hcl, true)
                              
                              if session.object_positions.mu and session.object_positions.cs and session.object_positions.hcl then
                                    session.current_heist, session.initialized = "rats", true
                              else
                                  log("[BCCFS] Couldn't generate waypoint positions")
                              end
                        end
                        
                  elseif heist == "nail" then
                        session.current_heist, session.initialized = "lab_rats", true
                        
                  else
                        session.current_heist, session.initialized = "other", true
                  end
            end
      end
end

function BCCFSClass:send_to_client(ingredient, object, position)
      if BCCFSMenu._data.toggle_show_waypoint_value == true then
            BCCFSClass:_add_waypoint(object, position)
      end
      
      if BCCFSMenu._data.toggle_chat_message_value == true then
            BCCFSClass:print_ingredient(ingredient)
      end
end

local _queue_dialog_orig = DialogManager.queue_dialog

function DialogManager:queue_dialog(id, ...)
      local dialog_table = BCCFSClass.Common.Dialogs

      if dialog_table[id] then
            local batch = BCCFSClass.MethBatch
            
            if not session.initialized then
                  BCCFSClass:init()
            end
      
            -- If heist is Rats, Cook off or Lab Rats
            if session.current_heist == "rats" then
                  if dialog_table[id] == "Ingredient Added" or dialog_table[id] == "You messed up" then
                        if batch.last_ingredient_added == "Muriatic Acid" then
                              batch.muriatic_acid = false
                              managers.hud:remove_waypoint("CookingWaypoint_CookThis_".. idstring.muriatic_acid)
                              
                        elseif batch.last_ingredient_added == "Caustic Soda" then
                              batch.caustic_soda = false
                              managers.hud:remove_waypoint("CookingWaypoint_CookThis_".. idstring.caustic_soda)
                              
                        elseif batch.last_ingredient_added == "Hydrogen Chloride" then
                              batch.hydrogen_chloride = false
                              managers.hud:remove_waypoint("CookingWaypoint_CookThis_".. idstring.hydrogen_chloride)
                        end
                        
                  elseif dialog_table[id] == "Muriatic Acid" and not batch.muriatic_acid then
                        batch.muriatic_acid = true
                        batch.last_ingredient_added = "Muriatic Acid"
                        BCCFSClass:send_to_client("Muriatic Acid", idstring.muriatic_acid, session.object_positions.mu)
                        
                  elseif dialog_table[id] == "Caustic Soda" and not batch.caustic_soda then
                        batch.caustic_soda = true
                        batch.last_ingredient_added = "Caustic Soda"
                        BCCFSClass:send_to_client("Caustic Soda", idstring.caustic_soda, session.object_positions.cs)
                        
                  elseif dialog_table[id] == "Hydrogen Chloride" and not batch.hydrogen_chloride then
                        batch.hydrogen_chloride = true
                        batch.last_ingredient_added = "Hydrogen Chloride"
                        BCCFSClass:send_to_client("Hydrogen Chloride", idstring.hydrogen_chloride, session.object_positions.hcl)
                  end
            elseif session.current_heist == "lab_rats" then
                  local static_positions = BCCFSClass.Common.Variables.receptacle_locations
                  
                  if dialog_table[id] == "Batch done" or dialog_table[id] == "You messed up" then
                        if batch.last_ingredient_added == "Muriatic Acid" then
                              managers.hud:remove_waypoint("CookingWaypoint_CookThis_".. idstring.muriatic_acid)
                              
                        elseif batch.last_ingredient_added == "Caustic Soda" then
                              managers.hud:remove_waypoint("CookingWaypoint_CookThis_".. idstring.caustic_soda)
                              
                        elseif batch.last_ingredient_added == "Hydrogen Chloride" then
                              managers.hud:remove_waypoint("CookingWaypoint_CookThis_".. idstring.hydrogen_chloride)
                        end
                        
                  elseif dialog_table[id] == "Muriatic Acid" then
                        if batch.last_ingredient_added == "Caustic Soda" then
                              managers.hud:remove_waypoint("CookingWaypoint_CookThis_".. idstring.caustic_soda)
                              
                        elseif batch.last_ingredient_added == "Hydrogen Chloride" then
                              managers.hud:remove_waypoint("CookingWaypoint_CookThis_".. idstring.hydrogen_chloride)
                        end
                        
                        --Send ingredient to client
                        BCCFSClass:send_to_client("Muriatic Acid", idstring.muriatic_acid, static_positions.mu_position)
                        batch.last_ingredient_added = "Muriatic Acid"
                        
                  elseif dialog_table[id] == "Caustic Soda" then
                        if batch.last_ingredient_added == "Muriatic Acid" then
                              managers.hud:remove_waypoint("CookingWaypoint_CookThis_".. idstring.muriatic_acid)
                              
                        elseif batch.last_ingredient_added == "Hydrogen Chloride" then
                              managers.hud:remove_waypoint("CookingWaypoint_CookThis_".. idstring.hydrogen_chloride)
                        end
                        
                        --Send ingredient to client
                        BCCFSClass:send_to_client("Caustic Soda", idstring.caustic_soda, static_positions.cs_position)
                        batch.last_ingredient_added = "Caustic Soda"
                  
            elseif dialog_table[id] == "Hydrogen Chloride" then
                        if batch.last_ingredient_added == "Muriatic Acid" then
                              managers.hud:remove_waypoint("CookingWaypoint_CookThis_".. idstring.muriatic_acid)
                              
                        elseif batch.last_ingredient_added == "Caustic Soda" then
                              managers.hud:remove_waypoint("CookingWaypoint_CookThis_".. idstring.caustic_soda)
                        end
                        
                        --Send ingredient to client
                        BCCFSClass:send_to_client("Hydrogen Chloride", idstring.hydrogen_chloride, static_positions.hcl_position)
                        batch.last_ingredient_added = "Hydrogen Chloride"
                  end
            end
      end
      
      return _queue_dialog_orig(self, id, ...)
end
