_G.BCCFSCommon = {}
local BCV = _G.BCCFSCommon

BCV.Dialogs = {
      -- Primary Dialogs
      ["pln_rt1_20"] = "Muriatic Acid",
      ["pln_rt1_22"] = "Caustic Soda",
      ["pln_rt1_24"] = "Hydrogen Chloride",
      
      ["pln_rat_stage1 20"] = "Muriatic Acid",
      ["pln_rat_stage1_22"] = "Caustic Soda",
      ["pln_rat_stage1_24"] = "Hydrogen Chloride",
      
      ["Play_pln_nai_15"] = "You messed up",
      ["pln_rat_stage1_26"] = "You messed up",
      
      -- Rats/Cook off only
      ["pln_rt1_12"] = "Ingredient Added",
      
      -- Lab Rats only
      ["Play_pln_nai_09"] = "Batch done"
}

BCV.Variables = {
      -- Global variables      
      ["object_ids"] = {
            -- idstrings
            ["muriatic_acid"] = "@ID4111102",
            ["caustic_soda"] = "@ID498cdfa",
            ["hydrogen_chloride"] = "@IDdafe5d9"
      },
      
      ["receptacle_locations"] = {
            -- Static world locations (For lab rats waypoints)
            ["mu_position"] = "157.121475,-635.353210,1863.831177",
            ["cs_position"] = "-3790.888916,450.120789,2049.024658",
            ["hcl_position"] = "-5518.002441,-555.464966,1236.319580"
      }
}
